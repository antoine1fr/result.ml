module type Basic = sig
  include Functor.S

  val pure : 'a -> 'a t
  val apply : ('a -> 'b) t -> 'a t -> 'b t
end

module type Basic2 = sig
  include Functor.S2

  val pure : 'a -> ('error, 'a) t
  val apply : ('error, ('a -> 'b)) t -> ('error, 'a) t -> ('error, 'b) t
end

module type Infix = sig
  include Functor.Infix
  val (<*>) : ('a -> 'b) t -> 'a t -> 'b t
end

module type Infix2 = sig
  include Functor.Infix2
  val (<*>) : ('error, ('a -> 'b)) t -> ('error, 'a) t -> ('error, 'b) t
end

module type S = sig
  include Basic
  module Infix : Infix with type 'a t := 'a t
end

module type S2 = sig
  include Basic2
  module Infix : Infix2 with type ('a, 'b) t := ('a, 'b) t
end

module Make (Basic : Basic) : S
  with type 'a t := 'a Basic.t
= struct
  let apply = Basic.apply
  let fmap = Basic.fmap
  let pure = Basic.pure

  module Infix = struct
    let ( <$> ) = fmap
    let ( <*> ) = apply
  end
end

module Make2 (Basic : Basic2) : S2
  with type ('error, 'a) t := ('error, 'a) Basic.t
= struct
  let apply = Basic.apply
  let fmap = Basic.fmap
  let pure = Basic.pure

  module Infix = struct
    let ( <$> ) = fmap
    let ( <*> ) = apply
  end
end
