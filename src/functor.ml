module type S = sig
  type 'a t
  val fmap : ('a -> 'b) -> 'a t -> 'b t
end

module type S2 = sig
  type ('a, 'b) t
  val fmap : ('a -> 'b) -> ('c, 'a) t -> ('c, 'b) t
end

module type Infix = sig
  type 'a t
  val (<$>) : ('a -> 'b) -> 'a t -> 'b t
end

module type Infix2 = sig
  type ('a, 'b) t
  val (<$>) : ('a -> 'b) -> ('c, 'a) t -> ('c, 'b) t
end
