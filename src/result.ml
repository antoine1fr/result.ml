module type S = sig
  type 'a error
  type ('a, 'b) t = [`Error of 'a error | `Ok of 'b]


  include Applicative.S2 with type ('a, 'b) t := ('a, 'b) t
end

module Make (Error : Monoid.S) : S
  with type 'a error := 'a Error.t
  (* and type ('a, 'b) t = ('a, 'b) t *)
= struct
  type ('a, 'b) t = [`Error of 'a Error.t | `Ok of 'b]

  include Applicative.Make2(struct
    type ('a, 'b) t = [`Error of 'a Error.t | `Ok of 'b]

    let pure x = `Ok x

    let fmap f = function
      | `Ok x -> `Ok (f x)
      | (`Error _) as x -> x

    let apply wrapped_f wrapped_x =
      match (wrapped_f, wrapped_x) with
      | (`Error err1, `Error err2) -> `Error (Error.op err1 err2)
      | (`Error _ as err, _) -> err
      | (_, (`Error _ as err)) -> err
      | (`Ok f, `Ok x) -> `Ok (f x)
  end)
end
