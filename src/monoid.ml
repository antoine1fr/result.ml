module type S = sig
  include Semigroup.S
  val unit : 'a t
end
