module R = Result.Make(struct
  type 'a t = 'a list

  let unit = []
  let op = List.append
end)

let _ = `Ok 42
let _ = `Error ["this is an error"]

let () =
  let open R.Infix in
  let result = print_endline <$> `Error ["don't like it"] in
  match result with
  | `Error error_list ->
    List.iter (Printf.fprintf stderr "Fatal error: %s\n") error_list
  | `Ok () -> ()

let () =
  let open R.Infix in
  let result = print_endline <$> `Ok "Kikoou!!" in
  match result with
  | `Error error_list ->
    List.iter (Printf.fprintf stderr "Fatal error: %s\n") error_list
  | `Ok () -> ()

let () =
  let open R.Infix in
  let result = (+) <$> `Ok 40 <*> `Ok 2 in
  match result with
  | `Error error_list ->
    List.iter (Printf.fprintf stderr "Fatal error: %s\n") error_list
  | `Ok n -> Printf.printf "%n\n" n

let () =
  let open R.Infix in
  let result = (+) <$> `Error ["couldn't read integer"] <*> `Ok 2 in
  match result with
  | `Error error_list ->
    List.iter (Printf.fprintf stderr "Fatal error: %s\n") error_list
  | `Ok n -> Printf.printf "%n\n" n

let () =
  let open R.Infix in
  let result = (+)
    <$> `Error ["couldn't read integer"]
    <*> `Error ["couldn't read integer"] in
  match result with
  | `Error error_list ->
    List.iter (Printf.fprintf stderr "Fatal error: %s\n") error_list
  | `Ok n -> Printf.printf "%n\n" n
