module type S = sig
  type 'a t
  val op : 'a t -> 'a t -> 'a t
end
